package it.unibo.ds.jade.behaviours;

import alice.tucson.asynchSupport.actions.AbstractTucsonAction;
import alice.tucson.asynchSupport.actions.AbstractTucsonOrdinaryAction;
import alice.tucson.service.TucsonOpCompletionEvent;
import it.unibo.tucson.jade.coordination.TucsonOpResult;
import it.unibo.tucson.jade.glue.BridgeToTucson;
import jade.core.ServiceException;
import jade.core.behaviours.Behaviour;

public class InvokeTucsonOperation extends Behaviour {

    private final AbstractTucsonOrdinaryAction operation;
    private TucsonOpCompletionEvent result;
    private final BridgeToTucson bridge;

    public InvokeTucsonOperation(BridgeToTucson bridge, AbstractTucsonOrdinaryAction operation) {
        this.operation = operation;
        this.bridge = bridge;
    }

    @Override
    public final void action() {
        result = syncInvoke(operation);

        if (result != null) {
            handleResult(result);
        } else {
            block();
        }
    }

    protected void handleResult(TucsonOpCompletionEvent result) {

    }

    @Override
    public final boolean done() {
        return result != null;
    }

    protected TucsonOpCompletionEvent syncInvoke(AbstractTucsonAction op) {
        try {
            return bridge.synchronousInvocation(op, null, this);
        } catch (ServiceException e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        }
    }
}
