package it.unibo.ds.jade.tucson4jade;

import alice.logictuple.LogicTuple;
import alice.tucson.asynchSupport.actions.ordinary.In;
import alice.tucson.asynchSupport.actions.ordinary.Out;
import alice.tucson.service.TucsonOpCompletionEvent;
import alice.tuprolog.Term;
import it.unibo.ds.jade.behaviours.InvokeTucsonOperation;

import java.util.Arrays;
import java.util.Iterator;

public class Worker extends Tucson4JadeAgent {

    @Override
    protected void setup() {
        super.setup();

        addBehaviour(waitForTasks());
    }

    private InvokeTucsonOperation waitForTasks() {
        return new InvokeTucsonOperation(getBridge(), new In(getTcid(), tuple("task(Name, Args)"))) {
            @Override
            protected void handleResult(TucsonOpCompletionEvent result) {
                log("Found task: %s", result.getTuple());
                if (result.getTuple().getArg(0).getName().equalsIgnoreCase("count_vowels")) {
                    addBehaviour(handleCountVowelsTask(result.getTuple()));
                } else {
                    addBehaviour(handleNotSupportedTask(result.getTuple()));
                }
            }
        };
    }

    private InvokeTucsonOperation handleCountVowelsTask(LogicTuple args) {
        int totalCount = 0;
        for (Iterator<? extends Term> i = args.getArg(1).listIterator(); i.hasNext(); ) {
            totalCount += countVowels(i.next().toString());
        }
        final LogicTuple result = new LogicTuple("result", args.getArg(0), args.getArg(1), tupleArg("%d", totalCount));

        return new InvokeTucsonOperation(getBridge(), new Out(getTcid(), result)) {
            @Override
            protected void handleResult(TucsonOpCompletionEvent r) {
                log("Sent result %s", result);
                addBehaviour(waitForTasks());
            }
        };
    }

    private final InvokeTucsonOperation handleNotSupportedTask(LogicTuple args) {
        log("Such an operation is not supported.");

        return new InvokeTucsonOperation(getBridge(), new Out(getTcid(), args)) {
            @Override
            protected void handleResult(TucsonOpCompletionEvent result) {
                log("Re-scheduled %s", args);
                addBehaviour(waitForTasks());
            }
        };
    }

    private static char[] vowels = new char[] {'a', 'e', 'i', 'o', 'u'};
    private static int countVowels(String str) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (Arrays.binarySearch(vowels, str.charAt(i)) >= 0) {
                count++;
            }
        }
        return count;
    }
}
