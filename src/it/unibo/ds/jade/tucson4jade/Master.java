package it.unibo.ds.jade.tucson4jade;

import alice.logictuple.LogicTuple;
import alice.logictuple.TupleArgument;
import alice.tucson.asynchSupport.actions.ordinary.In;
import alice.tucson.asynchSupport.actions.ordinary.Out;
import alice.tucson.service.TucsonOpCompletionEvent;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.ds.jade.behaviours.InvokeTucsonOperation;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class Master extends Tucson4JadeAgent  {

    static private Stream<String> lines() {
        return DivineComedy.POEM_1.stream();
    }

    private Behaviour submitTask(Stream<String> lines) {
        final List<Term> argsList = lines.map(Struct::new).collect(Collectors.toList());
        if (argsList.size() > 0) {
            final Term[] argsArray = argsList.toArray(new Term[0]);
            final LogicTuple task = new LogicTuple("task", tupleArg("count_vowels"), new TupleArgument(new Struct(argsArray)));
            return new InvokeTucsonOperation(getBridge(), new Out(getTcid(), task)) {
                @Override
                protected void handleResult(TucsonOpCompletionEvent result) {
                    log("Submitted task: %s", result.getTuple());
                }
            };
        } else {
            return  null;
        }
    }

    private Behaviour consumeResult(final List<Integer> resultAccumulator, int target) {
        return new InvokeTucsonOperation(getBridge(), new In(getTcid(), tuple("result(TaskName, Args, Result)"))) {
            @Override
            public void onStart() {
                log("Waiting for a task result...");
            }

            @Override
            protected void handleResult(TucsonOpCompletionEvent result) {
                log("Found result: %s", result.getTuple());

                resultAccumulator.add(
                    result.getTuple().getArg(2).intValue()
                );
            }

            @Override
            public int onEnd() {
                if (resultAccumulator.size() >= target){
                    log("All results have been collected");
                    addBehaviour(
                        terminate(resultAccumulator.stream().mapToInt(i -> i).sum())
                    );
                }
                return super.onEnd();
            }
        };
    }

    private Behaviour terminate(int result) {
        return new OneShotBehaviour() {
            @Override
            public void action() {
                log("Final result is: %s", result);
                doDelete();
            }
        };
    }

    private Behaviour createTasks() {
        final List<Integer> resultAccumulator = new LinkedList<>();

        return new OneShotBehaviour() {
            @Override
            public void action() {
                final int chunkSize = 3;
                final List<String> lines = lines().collect(Collectors.toList());
                final int chunkCount = lines.size() / chunkSize + (lines.size() % chunkSize == 0 ? 0 : 1);

                LongStream.iterate(0, i -> i + chunkSize)
                    .mapToObj(i -> lines.stream().skip(i).limit(chunkSize))
                    .map(chunk -> submitTask(chunk))
                    .limit(chunkCount)
                    .filter(Objects::nonNull)
                    .peek(Master.this::addBehaviour)
                    .map(behaviour -> consumeResult(resultAccumulator, chunkCount))
                    .forEach(Master.this::addBehaviour);
            }
        };
    }

    @Override
    protected void setup() {
        super.setup();

        addBehaviour(createTasks());
    }
}
