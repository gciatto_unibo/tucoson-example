package it.unibo.ds.jade.tucson4jade;

import alice.logictuple.LogicTuple;
import alice.logictuple.TupleArgument;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.logictuple.exceptions.InvalidTupleArgumentException;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tuprolog.Term;
import it.unibo.tucson.jade.exceptions.CannotAcquireACCException;
import it.unibo.tucson.jade.glue.BridgeToTucson;
import it.unibo.tucson.jade.service.TucsonHelper;
import it.unibo.tucson.jade.service.TucsonService;
import jade.core.Agent;
import jade.core.ServiceException;

public class Tucson4JadeAgent extends Agent {
    private TucsonHelper helper;
    private BridgeToTucson bridge;
    private TucsonTupleCentreId tcid;

    public TucsonHelper getHelper() {
        return helper;
    }

    public BridgeToTucson getBridge() {
        return bridge;
    }

    public TucsonTupleCentreId getTcid() {
        return tcid;
    }

    protected void log(final String msg, Object... args) {
        System.out.printf("[" + this.getName() + "]: " + msg + "\n", args);
    }

    @Override
    protected void setup() {
        this.log("I'm started.");

        String netid = getArguments()[0].toString();
        String portString = getArguments()[1].toString();

        netid = netid != null ? netid : "localhost";
        portString = portString != null ? portString : "20504";

        int port = Integer.parseInt(portString);

        try {
            /*
             * First of all, get the helper for the service you want to exploit
             */
            this.helper = (TucsonHelper) this.getHelper(TucsonService.NAME);
            /*
             * Then, start a TuCSoN Node (if not already up) as the actual
             * executor of the service
             */
            if (!this.helper.isActive(netid, port, 10000)) {
                this.log("Booting local TuCSoN Node on default port...");
                this.helper.startTucsonNode(port);
            }
            /*
             * Obtain ACC (which is actually given to the bridge, not directly
             * to your agent)
             */
            this.helper.acquireACC(this);
            /*
             * Get the univocal bridge for the agent. Now, mandatory, set-up
             * actions have been carried out and you are ready to coordinate
             */
            this.bridge = this.helper.getBridgeToTucson(this);
            /*
             * build a tuple centre id
             */
            this.tcid = this.helper.buildTucsonTupleCentreId("default",netid, port);
        } catch (final ServiceException e) {
            this.log(">>> No TuCSoN service active, reboot JADE with -services it.unibo.tucson.jade.service.TucsonService option <<<");
            this.doDelete();
        } catch (final TucsonInvalidAgentIdException e) {
            this.log(">>> TuCSoN Agent ids should be compliant with Prolog sytnax (start with lowercase letter, no special symbols), choose another agent id <<<");
            this.doDelete();
        } catch (final TucsonInvalidTupleCentreIdException e) {
            // should not happen
            e.printStackTrace();
            this.doDelete();
        } catch (final CannotAcquireACCException e) {
            // should not happen
            e.printStackTrace();
            this.doDelete();
        } catch (final TucsonOperationNotPossibleException e) {
            this.log(">>> TuCSoN Node cannot be installed, check if given port is already in use <<<");
            this.doDelete();
        }
    }

    protected LogicTuple tuple(String format, Object... args) {
        try {
            return LogicTuple.parse(String.format(format, args));
        } catch (InvalidLogicTupleException e) {
            throw new RuntimeException(e);
        }
    }

    protected Term term(String format, Object... args) {
        return Term.createTerm(String.format(format, args));
    }

    protected TupleArgument tupleArg(String format, Object... args) {
        try {
            return TupleArgument.parse(String.format(format, args));
        } catch (InvalidTupleArgumentException e) {
            throw  new RuntimeException(e);
        }
    }

    @Override
    protected void takeDown() {
        this.log("I'm done.");
    }
}
