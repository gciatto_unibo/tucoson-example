:-initialization((agent_name(N), node_address(A), node_port(P), acquire_acc(N, A, P), write("ACC acquired"), nl)).

agent_name($agent_name). % TODO insert <surname>_<name> here
node_address('localhost').
node_port(20504).

send_message(Message, Receiver) :-
  agent_name(Me),
  out(message(Receiver, Message, Me)),
  write("["), write(Me), write("] "), write("Sent message <"), write(Message), write("> to agent "), write(Receiver), nl.

receive_message(Message, Sender) :-
  agent_name(Me),
  in(message(Me, Message, Sender)),
  write("["), write(Me), write("] "), write("Received message <"), write(Message), write("> from agent "), write(Sender), nl.