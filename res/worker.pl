:-initialization((agent_name(N), node_address(A), node_port(P), acquire_acc(N, A, P), write("ACC acquired"), nl)).

agent_name(worker).
node_address('localhost').
node_port(20504).

agent_execution :-
  agent_name(Name),
  node_address(Address),
  node_port(Port),
  acquire_acc(Name, Address, Port),
  write("Acquired ACC on node "), write(Address), write(":"), write(Port), write(" for agent "), write(Name), nl,
  (agent_loop; true),
  release_acc, !,
  write("Released ACC for agent "), write(Name), nl.
  
agent_loop :- !,
  agent_loop_step,
  agent_loop.
  
agent_loop_step :-
 write("Waiting for task..."), nl,
  in(task(Name, Args)),
  write("Found task "), write(task(Name, Args)), nl,
  handle_request(Name, Args).

handle_request(count_vowels, [X | Xs]) :-
	write("Supported task "), write(task(count_vowels, Args)), write(" ... "),
	count_vowels([X | Xs], R),
	write(" rusult: "), write(R), nl,
	out(result(count_vowels, [X | Xs], R)).

handle_request(Name, Args) :-
    write("Not supported task "), write(task(count_vowels, Args)), nl,
	out(task(Name, Args)),
	write("Re-scheduled task "), write(task(count_vowels, Args)), nl.

count_vowels(Lines, Count) :-
    count_vowels(Lines, Count, 0).
    
count_vowels([], C, C).
count_vowels([L | Ls], C, P) :-
    atom_chars(L, Chars),
	count_occurrences(Chars, [a, e, i, o, u], P1),
    P2 is P + P1,
    count_vowels(Ls, C, P2).
    
count_occurrences(L, Es, N) :-
  count_occurrences(L, Es, N, 0).

count_occurrences([], _, N, N).
count_occurrences([X | Xs], Es, C, N) :-
  member(X, Es), !,
  N1 is N + 1,
  count_occurrences(Xs, Es, C, N1).
count_occurrences([X | Xs], Es, C, N) :-
  count_occurrences(Xs, Es, C, N).
  

