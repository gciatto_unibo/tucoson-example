:-initialization((agent_name(N), node_address(A), node_port(P), acquire_acc(N, A, P), write("ACC acquired"), nl)).

agent_name($agent_name). % TODO insert responder_<surname> here
node_address('localhost').
node_port(20504).

agent_execution :-
  agent_name(Name),
  node_address(Address),
  node_port(Port),
  acquire_acc(Name, Address, Port),
  write("Acquired ACC on node "), write(Address), write(":"), write(Port), write(" for agent "), write(Name), nl,
  see(stdin),
  (agent_loop; true),
  seen(stdin),
  release_acc, !,
  write("Released ACC for agent "), write(Name), nl.

agent_loop :- !,
  agent_loop_step,
  agent_loop.

agent_loop_step :-
  agent_name(MyName),
  write("Waiting for a message..."), nl,
  in(message(MyName, Payload, Sender)),
  write("Received message: "), write(Payload), write(" from agent: "), write(Sender), nl,
  handle_message(Payload, Sender).

handle_message(Payload, Sender) :-
  write("Please provide a response message: "), nl,
  read(Response),
  agent_name(MyName),
  out(message(Sender, Response, MyName)),
  write("Sent answer: "), write(Response), write(" to agent: "), write(Sender), nl.